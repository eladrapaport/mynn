import numpy as np


def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def sigmoid_prime(z):
    return sigmoid(z) * (1 - sigmoid(z))

def cost_func_deriv(activations, y):
    return activations - y

class NeuralNetwork:
    def __init__(self, sizes):
        self.weights = [np.random.rand(y, x) for x, y in zip(sizes[:-1], sizes[1:])]
        self.biases = [np.random.rand(x) for x in sizes[1:]]

    def feedforward(self, X):
        for weight, bias in zip(self.weights, self.biases):
            X = sigmoid(weight.dot(X) + bias)
        return X

    def SGD(self, training_data, epochs, mini_batch_size, alpha, test_data=None):
        n = len(training_data)
        for epoch in epochs:
            np.random.shuffle(training_data)
            mini_batches = [
                training_data[k: k + mini_batch_size]
                for k in range(0, n, mini_batch_size)
            ]
            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, alpha)

            if test_data is not None:
                print('results for epoch %d: %s', epoch, self.evaluate(test_data))

    def update_mini_batch(self, mini_batch, alpha):
        dws = np.zeros(self.weights.shape)
        dbs = np.zeros(self.biases.shape)
        for x, y in mini_batch:
            dw, db = backprop(x, y)
            dws += dw
            dbs += db
        self.weights -= alpha / len(mini_batch) * dws
        self.biases -= alpha / len(mini_batch) * dbs

    def evaluate(self, data):
        results = [(np.argmax(self.feedforward(x)), y)
                   for (x, y) in data]
        amount_correct = sum(int(x == y) for x, y in results)
        return str(amount_correct) + ' / ' + str(len(data))

    def backprop(self, X, y):
        b_derivs = [np.zeros(bs.shape) for bs in self.biases]
        w_derivs = [np.zeros(ws.shape) for ws in self.weights]
        zees = []
        ayes = [X]
        for weight, bias in zip(self.weights, self.biases):
            X = weight.dot(X) + bias
            zees.append(X)
            X = sigmoid(X)
            ayes.append(X)
        output_err = cost_func_deriv(ayes[-1], y) * sigmoid_prime(zees[-1])




